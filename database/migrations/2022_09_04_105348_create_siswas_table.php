<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->id();
            $table->string('nama_siswa',50);
            $table->char('nik',20);
            $table->char('nis',20);
            $table->string('tempat_lahir',50);
            $table->date('tgl_lahir');
            $table->char('no_waSiswa',20);
            $table->text('alamat',200);
            $table->string('nama_ayah',50);
            $table->string('pekerjaan_ayah',100);
            $table->string('nama_ibu',50);
            $table->string('pekerjaan_ibu',100);
            $table->char('no_waOrangtua',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
